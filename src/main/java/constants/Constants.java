package constants;

public class Constants {

    public static final int IMPLICITLY_WAIT_TIMEOUT = 5;

    private Constants() {
    }
}
