package utils;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import static com.codeborne.selenide.WebDriverRunner.driver;

public class KeyboardUtils {

    private KeyboardUtils() {
    }

    public static void submit(SelenideElement field) {
        field.pressEnter();
    }

    public static void moveOutOfTheField(SelenideElement field) {
        field.pressTab();
    }
}
