package utils;

import com.codeborne.selenide.conditions.webdriver.UrlCondition;
import org.openqa.selenium.WebDriver;

public class UrlMatchesPattern extends UrlCondition {


    public UrlMatchesPattern(String urlPattern) {
        super("matches pattern ", urlPattern);
    }

    @Override
    public boolean test(WebDriver webDriver) {
        return webDriver.getCurrentUrl().matches(expectedUrl);
    }
}
