package utils;

import com.codeborne.selenide.SelenideElement;
import desktop.fragments.forms.FormField;

import java.util.Map;

public interface HasFormFields {

    Map<String, FormField> getFormFields();

    default void setInputValue(String inputFieldName, String value) {
        SelenideElement input = getFormFields().get(inputFieldName).getFieldLocator();
        input.setValue(value);
    }

    default String getErrorMessageOfInput(String inputFieldName) {
        return getFormFields().get(inputFieldName).getFieldErrorMessageLocator().getText();
    }

    default void setOptionByVisibleText(String selectFieldName, String option) {
        SelenideElement select = getFormFields().get(selectFieldName).getFieldLocator();
        select.selectOptionContainingText(option);
    }
}
