package utils;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static com.codeborne.selenide.WebDriverRunner.driver;
import static constants.Constants.IMPLICITLY_WAIT_TIMEOUT;

public class WaiterUtils {

    private static WebDriverWait waiter;

    public WaiterUtils() {
        waiter = new WebDriverWait(driver().getWebDriver(), 2);
    }

    public static void waitForPageReadyState() {
        new WebDriverWait(driver().getWebDriver(), IMPLICITLY_WAIT_TIMEOUT).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState")
                        .equals("complete"));
    }

    public static void waitForVisibilityOfElement(int timeToWait, SelenideElement element) {
        element.shouldBe(Condition.visible, Duration.ofSeconds(timeToWait));
    }
}
