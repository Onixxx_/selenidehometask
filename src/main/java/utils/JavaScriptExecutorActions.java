package utils;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.WebDriverRunner.driver;

public class JavaScriptExecutorActions {

    private JavaScriptExecutorActions() {
    }

    public static void clickButtonUsingJS(SelenideElement button) {
        JavascriptExecutor jse = (JavascriptExecutor)driver().getWebDriver();
        jse.executeScript("arguments[0].click();", button);
    }

    public static void scrollToElementJS(SelenideElement element) {
        JavascriptExecutor jse = (JavascriptExecutor)driver().getWebDriver();
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
