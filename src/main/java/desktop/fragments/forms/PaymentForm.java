package desktop.fragments.forms;

import abstract_classes.fragment.AbstractFragment;
import com.codeborne.selenide.SelenideElement;
import dto.Card;
import org.openqa.selenium.By;
import utils.HasFormFields;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.switchTo;

public class PaymentForm extends AbstractFragment implements HasFormFields {

    private final SelenideElement rootElement = $("div.card-form");
    private final SelenideElement cardNumber = $("input#credit-card-number");
    private final SelenideElement expiryDate = $("input#expiration");
    private final SelenideElement cvv = $("input#cvv");

    public PaymentForm() {
        setRootElement(rootElement);
    }

    @Override
    public Map<String, FormField> getFormFields() {
        Map<String, FormField> formFields = new HashMap<>();
        formFields.put("cardNumber", new FormField(cardNumber));
        formFields.put("expiryDate", new FormField(expiryDate));
        formFields.put("Cvv", new FormField(cvv));

        return formFields;
    }

    public List<String> getGlobalErrorMessages() {
        return Arrays.asList(getChildElement(getErrorMessages()).getText().split("\n"));
    }

    public void fillPaymentForm(Card cardInfo) {
        switchTo().frame("braintree-hosted-field-number");
        setInputValue("cardNumber", cardInfo.getCardNumber());

        switchTo().defaultContent();
        switchTo().frame("braintree-hosted-field-expirationDate");
        setInputValue("expiryDate", cardInfo.getExpiryMonth() + cardInfo.getExpiryYear());

        switchTo().defaultContent();
        switchTo().frame("braintree-hosted-field-cvv");
        setInputValue("Cvv", cardInfo.getCvv());

        switchTo().defaultContent();
    }

    private By getErrorMessages() {
        return By.cssSelector("div.buynow-error-msg");
    }
}
