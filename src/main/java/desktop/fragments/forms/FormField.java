package desktop.fragments.forms;

import com.codeborne.selenide.SelenideElement;

public class FormField {

    private final SelenideElement fieldLocator;
    private SelenideElement fieldErrorMessageLocator;

    public FormField(SelenideElement fieldLocator) {
        this.fieldLocator = fieldLocator;
    }

    public FormField(SelenideElement fieldLocator, SelenideElement fieldErrorMessageLocator) {
        this.fieldLocator = fieldLocator;
        this.fieldErrorMessageLocator = fieldErrorMessageLocator;
    }

    public SelenideElement getFieldLocator() {
        return fieldLocator;
    }

    public SelenideElement getFieldErrorMessageLocator() {
        return fieldErrorMessageLocator;
    }

}
