package desktop.fragments.forms;

import abstract_classes.fragment.AbstractFragment;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import utils.HasFormFields;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$;
import static constants.FilterConstants.*;

public class SearchFiltersForm extends AbstractFragment implements HasFormFields {

    private final SelenideElement rootElement = $("form.filter-menu");

    public SearchFiltersForm() {
        setRootElement(rootElement);
    }

    @Override
    public Map<String, FormField> getFormFields() {
        Map<String, FormField> formFields = new HashMap<>();
        formFields.put(PRICE, new FormField(getChildElement(getPrice())));
        formFields.put(AVAILABILITY, new FormField(getChildElement(getAvailability())));
        formFields.put(LANG, new FormField(getChildElement(getLanguage())));
        formFields.put(FORMAT, new FormField(getChildElement(getFormat())));
        return formFields;
    }

    public void selectFilters(Map<String, String> filters) {
        setOptionByVisibleText(PRICE, filters.get(PRICE));
        setOptionByVisibleText(AVAILABILITY, filters.get(AVAILABILITY));
        setOptionByVisibleText(LANG, filters.get(LANG));
        setOptionByVisibleText(FORMAT, filters.get(FORMAT));
        submitFilters();
    }

    private void submitFilters() {
        clickChildElement(getSubmitButton());
    }

    private By getPrice() {
        return By.cssSelector("select#filterPrice");
    }

    private By getAvailability() {
        return By.cssSelector("select#filterAvailability");
    }

    private By getLanguage() {
        return By.cssSelector("select#filterLang");
    }

    private By getFormat() {
        return By.cssSelector("select#filterFormat");
    }

    private By getSubmitButton() {
        return By.cssSelector("button[type='submit']");
    }
}
