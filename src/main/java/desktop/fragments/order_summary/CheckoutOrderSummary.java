package desktop.fragments.order_summary;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutOrderSummary extends AbstractOrderSummary {

    private final SelenideElement rootElement = $("div.mini-basket");

    public CheckoutOrderSummary() {
        setRootElement(rootElement);
    }

    @Override
    public Map<String, String> getTotals() {
        Map<String, String> totals = new HashMap<>();
        totals.put("Sub-total", getChildElement(getSubTotal()).getText());
        totals.put("Delivery", getChildElement(getDelivery()).getText());
        totals.put("VAT", getChildElement(getVAT()).getText());
        totals.put("Total", getChildElement(getTotal()).getText());
        return totals;
    }

    private By getSubTotal() {
        return By.cssSelector("div.wrapper dl:nth-child(2) dd");
    }

    private By getDelivery() {
        return By.cssSelector("div.wrapper dl:nth-child(3) dd");
    }

    private By getVAT() {
        return By.cssSelector("div.wrapper dl:nth-child(4) dd");
    }

    private By getTotal() {
        return By.cssSelector("div.wrapper dl:nth-child(5) dd");
    }
}
