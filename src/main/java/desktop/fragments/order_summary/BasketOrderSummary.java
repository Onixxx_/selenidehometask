package desktop.fragments.order_summary;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$;

public class BasketOrderSummary extends AbstractOrderSummary {

    private final SelenideElement rootElement = $("div.basket-totals");

    public BasketOrderSummary() {
        setRootElement(rootElement);
    }
    @Override
    public Map<String, String> getTotals() {
        Map<String, String> totals = new HashMap<>();
        totals.put("Delivery cost", getChildElement(getDeliveryCost()).getText());
        totals.put("Total", getChildElement(getTotal()).getText());
        return totals;
    }

    private By getDeliveryCost() {
        return By.cssSelector("dl.delivery-text dd");
    }

    private By getTotal() {
        return By.cssSelector("dl.total dd");
    }
}
