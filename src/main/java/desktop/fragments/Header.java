package desktop.fragments;

import abstract_classes.fragment.AbstractFragment;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static utils.KeyboardUtils.submit;

public class Header extends AbstractFragment {

    private final SelenideElement rootElement = $("header.header");

    public Header() {
        setRootElement(rootElement);
    }

    public void searchForProducts(String productName) {
        getChildElement(getSearchField()).setValue(productName);
        submit(getChildElement(getSearchField()));
    }

    private By getSearchField() {
        return By.cssSelector("input[name='searchTerm']");
    }

}
