package desktop.fragments;

import abstract_classes.fragment.AbstractFragment;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BasketPopup extends AbstractFragment {

    private final SelenideElement rootElement = $("div.modal-content");

    public BasketPopup() {
        setRootElement(rootElement);
    }

    public void proceedToBasketCheckout() {
        clickChildElement(getToBasketCheckoutButton());
    }

    private By getToBasketCheckoutButton() {
        return By.cssSelector("a[href='/basket']");
    }
}
