package desktop.pages;

import abstract_classes.page.AbstractPage;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import desktop.fragments.BasketPopup;
import desktop.fragments.forms.SearchFiltersForm;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$$;

public class SearchPage extends AbstractPage {

    private final String URL = "https://www.bookdepository.com/search?searchTerm=";

    private final String URL_PATTERN = "[https://www.bookdepository.com/search?searchTerm=].+";

    private ElementsCollection foundedProducts = $$("div.book-item h3.title a");

    private List<SelenideElement> addToBasketButton = $$("a.add-to-basket");

    public SearchPage() {
        setPageUrl(URL);
        setPageUrlPattern(URL_PATTERN);
    }

    public List<String> getFoundedProductsNames() {
        return foundedProducts.stream().map(SelenideElement::getText).collect(Collectors.toList());
    }

    public ElementsCollection getFoundedProducts() {
        return foundedProducts;
    }

    public SearchFiltersForm getSearchFiltersForm() {
        return new SearchFiltersForm();
    }

    public BasketPopup getBasketPopup() {
        return new BasketPopup();
    }

    public void clickAddProductToBasket(String productName) {
        int index = getFoundedProductsNames().indexOf(productName);
        addToBasketButton.get(index).click();
    }
}