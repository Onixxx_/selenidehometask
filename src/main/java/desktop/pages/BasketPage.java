package desktop.pages;

import abstract_classes.page.AbstractPage;
import com.codeborne.selenide.SelenideElement;
import desktop.fragments.order_summary.BasketOrderSummary;

import static com.codeborne.selenide.Selenide.$;
import static utils.JavaScriptExecutorActions.clickButtonUsingJS;

public class BasketPage extends AbstractPage {

    private SelenideElement checkoutButton = $("div.checkout-btns-wrap a.optimizely-variation-1");

    private final String URL = "https://www.bookdepository.com/basket";

    public BasketPage() {
        setPageUrl(URL);
    }

    public void proceedToCheckoutPayment() {
        clickButtonUsingJS(checkoutButton);
    }

    public BasketOrderSummary getOrderSummary() {
        return new BasketOrderSummary();
    }
}
