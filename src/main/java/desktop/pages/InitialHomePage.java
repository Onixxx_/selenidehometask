package desktop.pages;

import abstract_classes.page.AbstractPage;
import desktop.fragments.Header;

public class InitialHomePage extends AbstractPage {

    private final String URL = "https://www.bookdepository.com/";

    public InitialHomePage() {
        setPageUrl(URL);
    }

    public Header getHeader() {
        return new Header();
    }
}
