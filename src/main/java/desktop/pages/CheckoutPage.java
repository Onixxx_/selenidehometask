package desktop.pages;

import abstract_classes.page.AbstractPage;
import com.codeborne.selenide.SelenideElement;
import desktop.fragments.forms.DeliveryAddressForm;
import desktop.fragments.forms.PaymentForm;
import desktop.fragments.order_summary.CheckoutOrderSummary;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage extends AbstractPage {

    private final String URL = "https://www.bookdepository.com/payment";

    private final String URL_PATTERN = "[https://www.bookdepository.com/payment].+";

    private final SelenideElement buyNowButton = $("button[type='submit']");

    public CheckoutPage() {
        setPageUrl(URL);
        setPageUrlPattern(URL_PATTERN);
    }

    public void clickBuyNowButton() {
        buyNowButton.scrollIntoView(true);
        buyNowButton.click();
    }

    public DeliveryAddressForm getDeliveryAddressForm() {
        return new DeliveryAddressForm();
    }

    public PaymentForm getPaymentForm() {
        return new PaymentForm();
    }

    public CheckoutOrderSummary getOrderSummary() {
        return new CheckoutOrderSummary();
    }
}
