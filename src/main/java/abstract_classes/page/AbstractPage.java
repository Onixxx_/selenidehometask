package abstract_classes.page;

import com.codeborne.selenide.WebDriverConditions;
import com.codeborne.selenide.impl.Waiter;
import utils.UrlMatchesPattern;

import static com.codeborne.selenide.WebDriverRunner.driver;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public abstract class AbstractPage {

    private String pageUrl;
    private String pageUrlPattern;

    protected AbstractPage() {
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrlPattern(String pageUrlPattern) {
        this.pageUrlPattern = pageUrlPattern;
    }

    public String getPageUrlPattern() {
        return pageUrlPattern;
    }

    public void checkUrl() {
        Waiter wait = new Waiter();
        if (isNotEmpty(pageUrlPattern)) {
            wait.wait(driver(), driver().getWebDriver(), new UrlMatchesPattern(pageUrlPattern));
        }
        else {
            wait.wait(driver(), driver().getWebDriver(), WebDriverConditions.url(pageUrl));
        }
    }
}