package abstract_classes.fragment;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import utils.WaiterUtils;

public abstract class AbstractFragment extends WaiterUtils {

    private SelenideElement rootElement;

    protected AbstractFragment() {
    }

    public void setRootElement(SelenideElement element) {
        this.rootElement = element;
    }

    public SelenideElement getRootElement() {
        return rootElement;
    }

    public void clickChildElement(By element) {
        rootElement.$(element).click();
    }

    public SelenideElement getChildElement(By element) {
        return rootElement.$(element);
    }
}
