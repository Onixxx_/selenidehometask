$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/DesktopCheckoutForGuestUser.feature");
formatter.feature({
  "name": "Desktop Checkout for Guest User",
  "description": "  As a customer\n  I want to be able proceed to checkout\n  So that I can specify my delivery and payment details and place the order",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.scenario({
  "name": "Proceed to checkout, final review and place order as guest user",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am an anonymous customer with clear cookies",
  "keyword": "Given "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.clearCookies()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I open the \"Initial home\" page",
  "keyword": "When "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.openPage(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I search for \"Thinking in Java\"",
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.search(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am redirected to the \"Search\" page",
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.isRedirectedToPage(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Search results contain the following products",
  "rows": [
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.searchResultsContainTheFollowingProducts(java.util.List\u003cjava.lang.String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I apply the following search filters",
  "rows": [
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.applyTheFollowingSearchFilters(java.util.Map\u003cjava.lang.String, java.lang.String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Search results contain only the following products",
  "rows": [
    {},
    {},
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.searchResultsContainOnlyTheFollowingProducts(java.util.List\u003cjava.lang.String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click \u0027Add to basket\u0027 button for product with name \"Thinking in Java\"",
  "keyword": "When "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.clickAddToBasketButtonForProductWithName(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select \u0027Basket\\Checkout\u0027 in basket pop-up",
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.selectBasketCheckoutInBasketPopUp()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am redirected to the \"Basket\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.isRedirectedToPage(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Basket order summary is as following:",
  "rows": [
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.basketOrderSummaryIsAsFollowing(java.util.Map\u003cjava.lang.String, java.lang.String\u003e)"
});
formatter.result({
  "error_message": "org.opentest4j.AssertionFailedError: Basket order summary is not as expected\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat stepDefs.CheckoutStepDefinitions.basketOrderSummaryIsAsFollowing(CheckoutStepDefinitions.java:124)\r\n\tat ✽.Basket order summary is as following:(file:///C:/Users/RAZER/source/java/selenidehometask/src/test/resources/features/DesktopCheckoutForGuestUser.feature:29)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I click \u0027Checkout\u0027 button on \u0027Basket\u0027 page",
  "keyword": "When "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.clickCheckoutButtonOnBasketPage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I am redirected to the \"Checkout\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.isRedirectedToPage(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click \u0027Buy now\u0027 button",
  "keyword": "When "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.clickBuyNowButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the following validation error messages are displayed on \u0027Delivery Address\u0027 form:",
  "rows": [
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.validationErrorMessagesAreDisplayedOnDeliveryAddressForm(java.util.Map\u003cjava.lang.String, java.lang.String\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the following validation error messages are displayed on \u0027Payment\u0027 form:",
  "rows": [
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.theFollowingValidationErrorMessagesAreDisplayedOnPaymentForm(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Checkout order summary is as following:",
  "rows": [
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.checkoutOrderSummaryIsAsFollowing(java.util.Map\u003cjava.lang.String, java.lang.String\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I checkout as a new customer with email \"test@user.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.checkoutAsANewCustomerWithEmail(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I fill delivery address information manually:",
  "rows": [
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.fillDeliveryAddressInformationManually(dto.DeliveryAddressInformation)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "there is no validation error messages displayed on \u0027Delivery Address\u0027 form",
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.thereIsNoValidationErrorMessagesDisplayedOnDeliveryAddressForm()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I enter my card details",
  "rows": [
    {},
    {},
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.enterCardDetails(java.util.Map\u003cjava.lang.String, java.lang.String\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png", "Proceed to checkout, final review and place order as guest user");
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Proceed to Home Page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I open \"https://www.bookdepository.com/\" page and check header is \"Book Depository: Free delivery worldwide on over 20 million books\" in \"Proceed to Home Page\"",
  "keyword": "Given "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.openUrlVerifyHeader(java.lang.String,java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded1.png", "Proceed to Home Page");
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Proceed to Category Page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I open \"https://www.bookdepository.com/category/2978/Humour\" page and check header is \"Comedy Books | Book Depository\" in \"Proceed to Search Page\"",
  "keyword": "Given "
});
formatter.match({
  "location": "stepDefs.CheckoutStepDefinitions.openUrlVerifyHeader(java.lang.String,java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded2.png", "Proceed to Category Page");
formatter.after({
  "status": "passed"
});
});